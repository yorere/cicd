const path = require('path');
const os = require('os')
// 存在源码的目录，流水线参数传入
const REPO_DIR = path.resolve(__dirname, "..", "..", "..", "..", "repo")
const BUNDLE_DIR = path.resolve(__dirname, "..", "..", "..", "..", "bundle")
const CSV_PATH = path.resolve(__dirname, "..", "..", "..", "..", "matrix_product.csv")
const DEFAULT_XML = path.resolve(__dirname, "..", "..", "..", "..", "default.xml")
const DEFAULT_CONFIG_DIR = path.resolve(os.homedir(), '.hpm')
const USER_JSON = path.resolve(__dirname, "..", "config", "user.json")

module.exports = {
  REPO_DIR,
  BUNDLE_DIR,
  CSV_PATH,
  DEFAULT_XML,
  DEFAULT_CONFIG_DIR,
  USER_JSON
};
