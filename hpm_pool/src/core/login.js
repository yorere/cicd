
const fs = require('fs');
const { USER_JSON } = require('../constant');
const axios = require('axios');
const shelljs = require('shelljs');
const Crypto = require('../utils/crypto');

async function getTokenLoginCookie(loginUrl, { ignoreCache } = {}) {
  if (!ignoreCache) {
    const cachedCookie = getCachedCookie();
    if (cachedCookie) {
      return cachedCookie;
    }
  }
  clearCachedCookie();

  const authorization = {
    timestamp: new Date().getTime(),
    user: "8EBENJ2",
    nonce: Math.floor(Math.random() * 1000000),
    version: 'v1',
    pk: Crypto.getKey('public').toString('utf8'),
  };

  // 因为json序列化和反序列化不能保证字段的顺序，所以不采用JSON.stringify(authorization)作为签名数据
  const originSignMsg = authorization.timestamp + authorization.user + authorization.nonce
    + authorization.version + authorization.pk;
  const authParam = {
    authorization,
    signature: Crypto.sign(originSignMsg),
  };
  const res = await axios.get(`${loginUrl}?authorization_code=${encodeURIComponent(JSON.stringify(authParam))}`);
  console.log(res)
  if (res && res.headers['set-cookie']) {
    console.log(res);
    const cookie = (res.headers['set-cookie'] || []).join(';');

    if (cookie) {
      setCachedCookie(cookie);
      return cookie;
    }
  }
  throw res;
}

function getCachedCookie() {
  if (fs.existsSync(USER_JSON)) {
    const data = JSON.parse(fs.readFileSync(USER_JSON, 'utf-8'))
    return data.cookie;
  }
  return '';
}

function setCachedCookie(cookie) {
  const json = {
    user: "8EBENJ2",
    cookie,
  };
  fs.writeFileSync(USER_JSON, JSON.stringify(json))
}

function clearCachedCookie() {
  shelljs.rm('-rf', USER_JSON)
}

function getCsrfToken(cookie) {
  if (cookie) {
    const cookieList = cookie.split(';').map((c) => c.trim());
    const csrfTokenMark = 'hpmCsrfToken=';
    const csrfToken = cookieList.find((c) => c.startsWith(csrfTokenMark));
    if (csrfToken) {
      return csrfToken.substr(csrfTokenMark.length);
    }
  }
  return '';
}

module.exports = {
  getCachedCookie,
  getCsrfToken,
  getTokenLoginCookie,
  setCachedCookie,
  clearCachedCookie
};
