const { REPO_DIR, DEFAULT_XML } = require("../constant")
const Git = require("../utils/git")
const Default_Xml = require("../utils/xml")

async function getMatchedRepos() {
    const [codeBranch = "OpenHarmony-3.0-LTS"] = process.argv.slice(2)
    const default_xml = await Default_Xml.fromPath(DEFAULT_XML)
    for (const { name: repo } of default_xml.xmlData.slice(0, 10)) {
        console.log(`cloning into ${repo}! `);
        await new Git(repo, REPO_DIR).clone(codeBranch)
        console.log(`${repo}: cloned successes! `);
    }
}

getMatchedRepos()