const { valid } = require("semver")
const fs = require('fs');
const { readJsonFile } = require('../utils');

const validator = {
  dirs(value) {
    if (Array.isArray(value)){
      return {}
    }
    return value
  },
  envs(value) {
    if (Array.isArray(value)){
      return {}
    }
    return value
  },
  segment(value, { destPath }) {
    if(Array.isArray(value)) {
      return { destPath }
    }
    if (!Object.keys(value).includes("destPath")) {
      return { destPath }
    }
    return value
  },
  version(value, { ver }) {
    if (valid(value)) {
      return value
    }
    return ver
  }
}

class Bundle {
  constructor(bundleJsonDir, otherData) {
    this.json = readJsonFile(bundleJsonDir)
    this.otherData = otherData
    this.bundleJsonDir = bundleJsonDir
  }
  validAndChange() {
    for (const key in this.json) {
      const callBack = validator[key]
      if (callBack && typeof callBack === "function") {
        this.json[key] = callBack(this.json[key], this.otherData)
      }
    }
    return this
  }
  toJson() {
    return this.json
  }
  saveToFile() {
    const data = JSON.stringify(this.toJson(), null, 4)
    fs.writeFileSync(this.bundleJsonDir, data)
  }
}

module.exports = {
  Bundle
};
