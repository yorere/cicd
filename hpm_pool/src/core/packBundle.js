const fs = require('fs');
const path = require('path');
const { Worker } = require('worker_threads');
const { REPO_DIR, DEFAULT_XML } = require("../constant")
const Default_Xml = require("../utils/xml")

async function Entry() {
  const [ version = "3.0.0-snapshot"] = process.argv.slice(2)
  const repos = fs.readdirSync(REPO_DIR, { encoding: 'utf-8' })
  const default_xml = await Default_Xml.fromPath(DEFAULT_XML)
  repos.forEach(repo => {
    const worker = new Worker(path.resolve(__dirname, "..", "thread", "index.js"), {
      workerData: {
        cwd: path.resolve(REPO_DIR, repo),
        version,
        destPath: default_xml.matchRepo(repo)
      }
    })
    worker.on("exit", exitCode => {
      if (exitCode===0){

      }
    })
  })
}

Entry()