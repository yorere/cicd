
const { workerData } = require('worker_threads');
const shelljs = require('shelljs');
const fs = require('fs');
const { Bundle } = require('../core/bundle');
const { BUNDLE_DIR } = require('../constant');
const path = require("path");

function packBundle({ cwd, version, destPath }) {
  const bundleJsonDir = path.resolve(cwd, "bundle.json")
  const readmeDir = path.resolve(cwd, "README.md")
  const licenseDir = path.resolve(cwd, "LICENSE")
  if (fs.existsSync(bundleJsonDir) && fs.existsSync(readmeDir) && fs.existsSync(licenseDir)) {
    new Bundle(bundleJsonDir, { ver: version, destPath }).validAndChange().saveToFile()
    shelljs.rm("-rf", [path.resolve(cwd , ".git"), path.resolve(cwd , ".gitee")])
    const child = shelljs.exec('hpm pack', { async: true, cwd })
    child.stdout.on("close", () => {
      shelljs.cp("-R", `${cwd}/*.tgz`, BUNDLE_DIR)
    })
  } else {
    console.log(`${cwd} pack failed: missing README.md or LICENSE`);
    process.exit(-1)
  }
}

packBundle(workerData)