const fs = require('fs');

function readJsonFile(path) {
  return JSON.parse(fs.readFileSync(path, 'utf-8'))
}

module.exports = {
  readJsonFile
};
