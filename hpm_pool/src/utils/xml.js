const fs = require('fs');
const { parseStringPromise } = require("xml2js");
class Default_Xml {
    xmlData;
    constructor(xmlPath) {
        this.xmlPath = xmlPath
    }
    parse() {
        const xmlData = fs.readFileSync(this.xmlPath).toString()
        return parseStringPromise(xmlData).then(res => res.manifest.project.map(item => item["$"]))
    }
    matchRepo(name) {
        const matchedLine = this.xmlData.find(data => data.name === name)
        if (matchedLine) {
            return matchedLine.path
        }
        return ""
    }
}
Default_Xml.fromPath = async xmlPath => {
    const default_xml = new Default_Xml(xmlPath)
    default_xml.xmlData = await default_xml.parse()
    return default_xml
}
module.exports = Default_Xml