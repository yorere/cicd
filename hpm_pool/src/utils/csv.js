const { parse } = require("csv-string")
const fs = require("fs");

class MatrixProduct {
    data;
    dataTitles;
    constructor(path) {
        this.path = path
    }
    parse(){
        const csvString = fs.readFileSync(this.path).toString()
        this.data = parse(csvString)
        this.dataTitles = this.data[0]
    }
    match(product, line) {
        const productIndex = this.dataTitles.findIndex(title => title === product)
        return line[productIndex] === "Y"
    }
    matchedRepos(product) {
        const repos = []
        this.data.slice(1).forEach(lines => {
            if (this.match(product, lines)) {
                repos.push(lines[0])
            }
        });
        return repos
    }
}

MatrixProduct.fromPath = path => {
    const matrixProduct = new MatrixProduct(path)
    matrixProduct.parse()
    return matrixProduct
}

module.exports = MatrixProduct