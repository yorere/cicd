const axios = require('axios');
const { v4: uuidv4 } = require('uuid');
const { getTokenLoginCookie, getCachedCookie, getCsrfToken } = require('../core/login');

const MAX_RETRY_TIMES = 3;

const http = axios.create({
  proxy: false,
});

http.interceptors.request.use((config) => {
    console.log(config.params);
    if (config.method === 'get' && config.params) {
    const keys = Object.keys(config.params);
    config.url += '?';
    keys.forEach((key) => {
      config.url += `${key}=${encodeURIComponent(config.params[key])}&`;
    });
    config.params = {};
  }

  if (config.method.toLowerCase() !== 'get') {
    config.headers['x-replay-context'] = `${new Date().getTime()}:${uuidv4()}`
  }

  if (config.needAuth) {
    config.headers.hpmAuth = true;
    const cookie = getCachedCookie();
    if (cookie) {
      const hpmCsrfToken = getCsrfToken(cookie);
      config.headers.Cookie = cookie;
      config.headers.hpmCsrfToken = hpmCsrfToken;
    }
  }
  return config;
});

function createAxiosResponseInterceptor() {
  const interceptor = http.interceptors.response.use(
    async (res) => {
      const { data } = res;
      if (data.code && data.code !== 200) {
        if (data.code === 302) {
          http.interceptors.response.eject(interceptor);
          await getTokenLoginCookie("https://hpm.harmonyos.com/hpm/auth/pk", { ignoreCache: true });
          createAxiosResponseInterceptor();
          const retryTime = res.config.retryTime || 0;
          if (retryTime < MAX_RETRY_TIMES) {
            res.config.retryTime = retryTime + 1;
            return http(res.config);
          }
        }
        throw data;
      }
      return res;
    },
    (error) => {
      throw error;
    }
  );
}

createAxiosResponseInterceptor();

module.exports = http;
